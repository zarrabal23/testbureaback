package com.example.demo.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.business.services.TestService;
import com.example.demo.dto.FileDTO;
import com.example.demo.dto.MarkDown;
import com.mongodb.client.gridfs.model.GridFSFile;

/**
 * 
 * @author Zarrabal
 *
 */
@RestController
@RequestMapping(value = "/api/v1/files")
public class TestController {
	protected static Logger log = LoggerFactory.getLogger(TestController.class);

	@Autowired
	private TestService testService;

	/**
	 * Service to get all files of the mongoDB
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> getFiles() {
		List<GridFSFile> gridFSFiles = testService.getFiles();
		
		List<FileDTO> result = FileDTO.mapFromListGridFs(gridFSFiles);

		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	/**
	 * Method used to get detail of the file
	 * 
	 * @param fileId
	 * @return
	 */
	@RequestMapping(value = "/detail", method = RequestMethod.GET)
	public ResponseEntity<?> getFileDetail(@RequestParam(required = true, value = "fileId") String fileId ) {
		
		GridFsResource file = testService.getFileDetail(fileId);
		FileDTO fileDTO = FileDTO.mapFileDetail(file);
		
		return new ResponseEntity<>(fileDTO, HttpStatus.OK);
	}
	
	/**
	 * Service used to upload a new file
	 * @param file
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) {
		List<FileDTO> result = new ArrayList<FileDTO>();
			
		try {
			List<GridFSFile> gridFSFiles = testService.uploadFile(file);
			result = FileDTO.mapFromListGridFs(gridFSFiles);
		} catch (Exception e) {
			HashMap<String, String> erros = new HashMap<>();
			erros.put("error", e.getMessage());
			return new ResponseEntity<>(erros, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

//	/**
//	 * Service used to deletea exist file
//	 * 
//	 * @param id
//	 */
//	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
//	public ResponseEntity<?> deleteFiles(@PathVariable("id") final String id) {
//
//		testService.deleteFile(id);
//
//		HashMap<String, String> result = new HashMap<>();
//		result.put("result", "File deleted succefully!!");
//
//		return new ResponseEntity<>(result, HttpStatus.OK);
//	}

	/**
	 * Método used to update a file
	 * 
	 * @param id
	 * @param file
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> uploadFile(@PathVariable("id") final String id, @RequestParam("file") MultipartFile file) {

		try {
			testService.updateFile(id, file);
		} catch (Exception e) {
			HashMap<String, String> erros = new HashMap<>();
			erros.put("error", e.getMessage());
			return new ResponseEntity<>(erros, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<>(null, HttpStatus.OK);
	}
	
	
	/**
	 * Service to get all files of the mongoDB
	 * @return
	 */
	@RequestMapping(value = "alldocuments", method = RequestMethod.GET)
	public ResponseEntity<?> getAll() {
		List<MarkDown> documents = testService.getAll();
		
		return new ResponseEntity<>(documents, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ResponseEntity<?> save(@RequestBody MarkDown markDown) {
	
		List<MarkDown> markDowns = new ArrayList<MarkDown>();
		
		try {
			markDowns = testService.save(markDown);
		} catch (Exception e) {
			HashMap<String, String> erros = new HashMap<>();
			erros.put("error", e.getMessage());
			return new ResponseEntity<>(erros, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(markDowns, HttpStatus.OK);
	}
	
	/**
	 * Method used to update a docuemnt
	 * @param fileId
	 * @param markDown
	 * @return
	 */
	@RequestMapping(value = "/update/{fileId}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<?> put(@PathVariable("fileId") final String fileId,
											@RequestBody MarkDown markDown) {

		List<MarkDown> markDowns = new ArrayList<MarkDown>();
		
		try {
			markDown.setId(fileId);
			markDowns = testService.update(markDown);
		} catch (Exception e) {
			HashMap<String, String> erros = new HashMap<>();
			erros.put("error", e.getMessage());
			return new ResponseEntity<>(erros, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<>(markDowns, HttpStatus.OK);
	}
	
	/**
	 * Service used to deletea exist file
	 * 
	 * @param id
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(@PathVariable("id") final String id) {

		List<MarkDown> markDowns = new ArrayList<MarkDown>();
		
		try {
			markDowns = testService.deleteFile(id);
		} catch (Exception e) {
			HashMap<String, String> erros = new HashMap<>();
			erros.put("error", e.getMessage());
			return new ResponseEntity<>(erros, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(markDowns, HttpStatus.OK);
	}
}