package com.example.demo.dao.repository;

import java.util.List;

import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dto.MarkDown;
import com.mongodb.client.gridfs.model.GridFSFile;

public interface TestRepository {

	/**
	 * Method used to get list of the files
	 * @return List<GridFSFile>
	 */
	public List<GridFSFile> getFiles();
	
	/**
	 * Method used to get detail of the file
	 * 
	 * @param fileId
	 * @return GridFSFile
	 */
	public GridFsResource getFileDetail(String fileId);
	
	/**
	 * Method used to upload a new file
	 * 
	 * @param file
	 * @throws Exception 
	 */
	public void uploadFile(MultipartFile file) throws Exception;
	
	/**
	 * Method to delete a file in MongoDB.
	 * 
	 * @param id
	 * @throws Exception 
	 */
	public void deleteFile(String id) throws Exception;
	
	/**
	 * Method to update a file in MongoDB.
	 * 
	 * @param id
	 * @param file
	 * @throws Exception 
	 */
	public void updateFile(String id, MultipartFile file) throws Exception;
	
	/**
	 * 
	 * @return
	 */
	public List<MarkDown> getAll();
	
	/**
	 * Method used to insert a new document
	 * @param markDown
	 * @throws Exception
	 */
	public void save(MarkDown markDown) throws Exception;
	
	/**
	 * Method used to update a document
	 * 
	 * @param markDown
	 * @throws Exception
	 */
	public void update(MarkDown markDown) throws Exception;
}
