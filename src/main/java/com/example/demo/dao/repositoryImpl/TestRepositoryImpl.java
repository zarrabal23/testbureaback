package com.example.demo.dao.repositoryImpl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dao.repository.CriteriaMongo;
import com.example.demo.dao.repository.TestRepository;
import com.example.demo.dto.MarkDown;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.gridfs.GridFSFindIterable;
import com.mongodb.client.gridfs.model.GridFSFile;

@Repository
public class TestRepositoryImpl implements TestRepository {
	private static Logger log = LoggerFactory.getLogger(TestRepositoryImpl.class);

	@Autowired
	private GridFsOperations gridFsOperations;
	
	@Autowired
	private MongoOperations mongoOperation;

	@Override
	public List<GridFSFile> getFiles() {
		List<GridFSFile> files = new ArrayList<>();

		try {
			Query queryConcept = new Query();
//			queryConcept.addCriteria(CriteriaMongo.where("internalCode").in(conceptInternalCodes));
			GridFSFindIterable iterableFiles = gridFsOperations.find(queryConcept);
			
			for (GridFSFile gridFSFile : iterableFiles) {
				files.add(gridFSFile);
			}

		} catch (Exception e) {
			log.info("Error in {} :: method:{} :: message:{}", this.getClass(), "getFiles", e.getMessage());
		}

		return files;
	}
	
	@Override
	public GridFsResource getFileDetail(String fileId) {
		GridFsResource resource = null;

		try {
			Query queryConcept = new Query();
			queryConcept.addCriteria(CriteriaMongo.where("_id").in(fileId));
			GridFSFile file = gridFsOperations.findOne(queryConcept);
			
			resource = gridFsOperations.getResource(file);

		} catch (Exception e) {
			log.info("Error in {} :: method:{} :: message:{}", this.getClass(), "getFileDetail", e.getMessage());
		}

		return resource;
	}

	@Override
	public void uploadFile(MultipartFile file) throws Exception {
		DBObject metaData = new BasicDBObject();
		metaData.put("fileName", file.getOriginalFilename());

		InputStream inputStream;
//		GridFSFile gridFSFile = null;

		try {
			inputStream = file.getInputStream();
			gridFsOperations.store(inputStream, file.getOriginalFilename(), file.getContentType(), metaData);

//			Query queryConcept = new Query();
//			queryConcept.addCriteria(CriteriaMongo.where("_id").in(id));
//			gridFSFile = gridFsOperations.findOne(queryConcept);

		} catch (IOException e) {
			log.info("Error in {} :: method:{} :: message:{}", this.getClass(), "uploadFile", e.getMessage());
			throw new Exception("Error to upload file");
		}

//		return gridFSFile;
	}

//	@Override
//	public void deleteFile(String id) {
//		GridFSFile result = gridFsOperations.findOne(new Query().addCriteria(CriteriaMongo.where("_id").is(id)));
//
//		if (result != null) {
//			try {
//				gridFsOperations.delete(new Query().addCriteria(CriteriaMongo.where("_id").is(id)));
//			} catch (Exception e) {
//				log.info("Error in {} :: method:{} :: message:{}", this.getClass(), "deleteFile", e.getMessage());
//			}
//		}
//	}
	
	@Override
	public void deleteFile(String id) throws Exception {
		Long exit = mongoOperation.count(new Query().addCriteria(CriteriaMongo.where("_id").is(id)), MarkDown.class);

		if (exit > 0) {
			try {
				mongoOperation.remove(new Query().addCriteria(CriteriaMongo.where("_id").is(id)), MarkDown.class);
			} catch (Exception e) {
				log.info("Error in {} :: method:{} :: message:{}", this.getClass(), "deleteFile", e.getMessage());
			}
		}else {
			throw new Exception("The file not exist");
		}
	}
	

	@Override
	public void updateFile(String id, MultipartFile file) throws Exception {
		DBObject metaData = new BasicDBObject();
		metaData.put("fileName", file.getOriginalFilename());

		InputStream inputStream;
		GridFSFile gridFSFile = null;
		GridFSFile result = gridFsOperations.findOne(new Query().addCriteria(CriteriaMongo.where("_id").is(id)));
		
		if (result != null) {
			try {
				
				gridFsOperations.delete(new Query().addCriteria(CriteriaMongo.where("_id").is(id)));
				
				//update file
				inputStream = file.getInputStream();
				
				ObjectId objectId = gridFsOperations.store(inputStream, file.getOriginalFilename(), file.getContentType(), metaData);

				Query queryConcept = new Query();
				queryConcept.addCriteria(CriteriaMongo.where("_id").in(objectId));
				gridFSFile = gridFsOperations.findOne(queryConcept);
				
			} catch (Exception e) {
				e.printStackTrace();
				throw new Exception("The record to delete not exist");
			}
		}else {
			throw new Exception("The record to delete not exist");
		}
	}
	
	
	@Override
	public List<MarkDown> getAll() {
		 List<MarkDown> documents = mongoOperation.findAll(MarkDown.class);
		return documents;
	}
	
	@Override
	public void save(MarkDown markDown) throws Exception {
		try {
			Long exit = mongoOperation.count(new Query().addCriteria(CriteriaMongo.where("fileName").is(markDown.getFileName())), MarkDown.class);
			
			if(exit.intValue() == 0) {
				mongoOperation.save(markDown);
			}else {
				throw new Exception("The file name exist");
			}
		} catch (Exception e) {
			log.info("Error in {} :: method:{} :: message:{}", this.getClass(), "save", e.getMessage());
			throw new Exception("Error to upload file");
		}

//		return gridFSFile;
	}

	@Override
	public void update(MarkDown markDown) throws Exception {
		try {
			MarkDown markDownDB = mongoOperation.findOne(new Query().addCriteria(CriteriaMongo.where("_id").is(markDown.getId())), MarkDown.class);
			
			if(markDownDB != null) {
				Long exit = mongoOperation.count(new Query().addCriteria(
							CriteriaMongo.where("fileName").is(markDown.getFileName()).and("_id").ne(markDown.getId())), 
							MarkDown.class);
				
				if(exit.intValue() == 0) {
					markDownDB.setFileName(markDown.getFileName());
					markDownDB.setContent(markDown.getContent());
					
					mongoOperation.save(markDownDB);
				}else {
					throw new Exception("The file name exist");
				}
			}else {
				throw new Exception("The file not exist");
			}
		} catch (Exception e) {
			log.info("Error in {} :: method:{} :: message:{}", this.getClass(), "save", e.getMessage());
			throw new Exception("Error to upload file");
		}
	}

}
