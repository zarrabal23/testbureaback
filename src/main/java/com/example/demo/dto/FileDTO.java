package com.example.demo.dto;

import java.io.IOException;
import java.io.Serializable;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.bson.BsonValue;
import org.bson.Document;
import org.springframework.data.mongodb.gridfs.GridFsResource;

import com.mongodb.client.gridfs.model.GridFSFile;


public class FileDTO implements Serializable {
	private static final long serialVersionUID = -3156199346816689818L;
	
	// Properties
	private String id;
	private String fileName;
	private String contentType;
	
	private String fileBase64;
	
	
	/**
	 * Method used to map an GridFSFileDTO entity List to a DTO List.
	 * 
	 * @param List<GridFSFile>
	 * @return List<GridFSFileDTO>
	 */
	public static List<FileDTO> mapFromListGridFs(List<GridFSFile> result) {
		return result.stream().map((gridFSFile) -> mapFromGridFS(gridFSFile)).collect(Collectors.toList());
	}

	// Methods
	public static FileDTO mapFromGridFS(GridFSFile gridFsFile) {
		FileDTO gridFSFileDTO = null;
		Document metadata = new Document();
		
		if (gridFsFile != null) {
			metadata = gridFsFile.getMetadata();
			gridFSFileDTO = new FileDTO();
			
			BsonValue objectId = gridFsFile.getId();
			
			gridFSFileDTO.setId(objectId.asObjectId().getValue().toString());
			gridFSFileDTO.setFileName(gridFsFile.getFilename());
			gridFSFileDTO.setContentType(metadata.get("_contentType").toString());
		}

		return gridFSFileDTO;
	}
	
	/**
	 * Method to map detail information of the file
	 * @param file
	 * @return
	 */
	public static FileDTO mapFileDetail(GridFsResource file) {
		FileDTO gridFSFileDTO = null;
		
		if (file != null) {
			gridFSFileDTO = new FileDTO();
			
			byte[] bytes;
			
			try {
				bytes = IOUtils.toByteArray(file.getInputStream());
				String image64 = "data:" + file.getContentType() + ";base64," + Base64.getEncoder().encodeToString(bytes);
				gridFSFileDTO.setFileBase64(image64);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return gridFSFileDTO;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileBase64() {
		return fileBase64;
	}

	public void setFileBase64(String fileBase64) {
		this.fileBase64 = fileBase64;
	}
}
