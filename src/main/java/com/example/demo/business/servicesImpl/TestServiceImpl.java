package com.example.demo.business.servicesImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.business.services.TestService;
import com.example.demo.dao.repository.TestRepository;
import com.example.demo.dto.MarkDown;
import com.mongodb.client.gridfs.model.GridFSFile;

@Repository
public class TestServiceImpl implements TestService {

	@Autowired
	private TestRepository testRepository;

	@Override
	public List<GridFSFile> getFiles() {
		List<GridFSFile> files = testRepository.getFiles();
		return files;
	}
	
	@Override
	public GridFsResource getFileDetail(String fileId) {
		GridFsResource file = testRepository.getFileDetail(fileId);
		
		return file;
	}

	@Override
	public List<GridFSFile> uploadFile(MultipartFile file) throws Exception {
		testRepository.uploadFile(file);
		return getFiles();
	}

	@Override
	public void updateFile(String id, MultipartFile file) throws Exception {
		try {
			testRepository.updateFile(id, file);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		
	}

	@Override
	public List<MarkDown> getAll() {
		return testRepository.getAll();
	}
	
	@Override
	public List<MarkDown> save(MarkDown markDown) throws Exception {
		List<MarkDown> markDowns = new ArrayList<MarkDown>();
		try {
			testRepository.save(markDown);
			markDowns = testRepository.getAll();
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		
		return markDowns;
	}

	@Override
	public List<MarkDown> update(MarkDown markDown) throws Exception {
		List<MarkDown> markDowns = new ArrayList<MarkDown>();
		
		testRepository.update(markDown);
		markDowns = testRepository.getAll();
		
		return markDowns;
	}

	@Override
	public List<MarkDown> deleteFile(String id) throws Exception {
		List<MarkDown> markDowns = new ArrayList<MarkDown>();
		
		testRepository.deleteFile(id);
		markDowns = testRepository.getAll();
		
		return markDowns;
	}
	
}
