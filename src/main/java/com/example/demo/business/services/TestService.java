package com.example.demo.business.services;

import java.util.List;

import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dto.MarkDown;
import com.mongodb.client.gridfs.model.GridFSFile;

public interface TestService {

	/**
	 * Method to get all files
	 * @return List<GridFSFile>
	 */
	public List<GridFSFile> getFiles();
	
	/**
	 * Method used to get detail of the file
	 * 
	 * @param fileId
	 * @return GridFSFile
	 */
	public GridFsResource getFileDetail(String fileId);
	
	/**
	 * Method used to upload a new file
	 * 
	 * @param file
	 * @return List<GridFSFile>
	 * @throws Exception 
	 */
	public List<GridFSFile> uploadFile(MultipartFile file) throws Exception;
	
	/**
	 * Method to update a file in MongoDB.
	 * 
	 * @param id
	 * @param file
	 * @throws Exception 
	 */
	public void updateFile(String id, MultipartFile file) throws Exception;
	
	
	/**
	 * Méthod to get all documents
	 * @return
	 */
	public List<MarkDown> getAll();
	
	/**
	 * Method used to insert a new document
	 * @param markDown
	 * @throws Exception
	 */
	public List<MarkDown> save(MarkDown markDown) throws Exception;
	
	/**
	 * Method used to update a document
	 * 
	 * @param markDown
	 * @throws Exception
	 */
	public List<MarkDown> update(MarkDown markDown) throws Exception;
	
	/**
	 * Method to delete a file in MongoDB.
	 * 
	 * @param id
	 * @throws Exception 
	 */
	public List<MarkDown> deleteFile(String id) throws Exception;
}
